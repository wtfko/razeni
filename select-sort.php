<?php
// vstup
$prvky = array(5, 6, 3, 1, 2, 4);

// prochazeni polem
$posledni_pozice = count($prvky) - 1;
for ($pozice = $posledni_pozice; $pozice >= 0; $pozice--) {
	// vyhledani nejvetsiho prvku
	$nejvetsi = null;
	for ($klic = 0; $klic < $pozice; $klic++) {
		if ($prvky[$klic] > $prvky[$pozice] && ($nejvetsi === null || $prvky[$klic] > $prvky[$nejvetsi])) {
			$nejvetsi = $klic;
		}
	}

	// prehozeni nejvetsiho prvku
	if ($nejvetsi !== null) {
		$docasny = $prvky[$pozice];
		$prvky[$pozice] = $prvky[$nejvetsi];
		$prvky[$nejvetsi] = $docasny;
	}
}
