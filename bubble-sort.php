<?php
// vstup
$prvky = array(5, 6, 3, 1, 2, 4);

// prochazeni polem
$pocet = count($prvky) - 1;
for ($krok = 0; $krok < $pocet; $krok++) {
	foreach ($prvky as $klic => $prvek) {
		$predchozi = $klic - 1;
		if (isset($prvky[$predchozi]) && $prvek < $prvky[$predchozi]) {
			$docasny = $prvky[$predchozi];
			$prvky[$predchozi] = $prvek;
			$prvky[$klic] = $docasny;
		}
	}
}
